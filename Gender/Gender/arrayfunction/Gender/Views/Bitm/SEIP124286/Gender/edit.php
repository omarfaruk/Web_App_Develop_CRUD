<?php
    include '../../../../Src/Bitm/SEIP124286/Gender/Gender.php';
    
    $obj = new Gender();
    $result = $obj->prepare($_GET)->show();
    
//    print_r($result); 
    
  if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

if(isset($result) && !empty($result)){

?>


<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8">
        <title> Update Name & Gender</title>
    </head>
    <body>
        <fieldset>
            <legend>Update Name & Gender </legend>
            <a href="index.php">Back to list</a>
            <form method="POST" action="update.php">
               
                <label>Update Name & Gender</label>
                <input type="hidden" name="id" value="<?php echo $_GET['id'];?>">
                <input type="text" name="name" value="<?php echo $result['name'];?>">
                <label>Select gender</label>
               
                <input type="radio" name="sex" value="Male" <?php echo ($result['sex']=='Male')?'checked':' ' ?>>male
                <input type="radio" name="sex" value="Female" <?php echo ($result['sex']=='Female')?'checked':'' ?>>Female
              
                <input type="submit" value="Update">
            
            </form>
        </fieldset>
    </body>
    
</html>

<?php } else {
    $_SESSION['Err_Msg'] = "You are going wrong way !". "<a href='create.php' >create</a>";
    header('location:errors.php');
    
} ?>
