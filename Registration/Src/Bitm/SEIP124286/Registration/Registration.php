<?php
namespace App\Bitm\SEIP124286\Registration;
use PDO;
class Registration{
    public $id='';
    public $user_name='';
    public $password='';
    public $repeat_password='';
    public $email='';
    public $varification_id='';
    public $is_active='';
    public $is_admin='';
    public $created='';
    public $modified='';
    public $deleted=''; 
    public $data='';
    public $dbuser='root';
    public $dbpass='';
    public $conn='';

    
     public function __construct() {
        session_start();
        
        $this->conn = new PDO('mysql:host=localhost;dbname=registration', $this->dbuser, $this->dbpass);
     // $conn =  mysql_connect('localhost','root','')Or die("OPPS! Connected Fail");
      //$db = mysql_select_db('php-26') or die("Database connect fail");
    
      
    }
    
    
    public function prepare($data=''){
           
//        echo " output from class";
//        print_r($data);
//        die();
       if(array_key_exists('username', $data) && !empty($data['username'])) {
           $this->user_name =$data['username'];
          
       }else {
            $_SESSION['user_name'] = "User Name must be required";
             header('location:create.php');
        }
        
       
          if(array_key_exists('password', $data) && !empty($data['password'])) {
           $this->password =$data['password'];
          
       }else {
            $_SESSION['password'] = "Password Must be required";
             header('location:create.php');
        }
        
       
      if(array_key_exists('re_pass', $data) && !empty($data['re_pass'])) {
           $this->repeat_password =$data['re_pass'];
          
       }else {
            $_SESSION['repeat_password'] = "Repeat Password required";
             header('location:create.php');
        }
        
       
        if(array_key_exists('email', $data) && !empty($data['email'])) {
           $this->email =$data['email'];
          
       }else {
            $_SESSION['email'] = "Email Must be required";
             header('location:create.php');
        }
       
       
        if(array_key_exists('id', $data)) {
           $this->id =$data['id']; 
       }
       if($this->password == $this->repeat_password){
//           $_SESSION['match']= "password match";
//           header('location:create.php');
       }else{
           $_SESSION['not_match'] = "password not match";
           header('location:create.php');
       }
       if(strlen($this->user_name)>=6 && strlen($this->user_name) <=12 ){
            
        }
        else {
             $_SESSION['length'] = "User Name Mustbe 6-12 Character";
            header('location:create.php');
        }
       
       
       return $this;
        
    }
    
    
    public function store(){
       
       if (!empty($this->user_name) && !empty($this->password) && !empty($this->repeat_password) && !empty($this->email)) {
           if($this->password == $this->repeat_password){
           if(strlen($this->user_name)<=12 && strlen($this->user_name)>=6 ){
       try {
             $query = "INSERT INTO td_registration (id, unique_id, verification_id, username, password, email, is_active, is_admin, created, modified, deleted) VALUES (:id, :unique_id, :verification_id, :username, :password, :email, :is_active, :is_admin, :created, :modified, :deleted)"; 
             $stmt = $this->conn->prepare($query);
             $stmt->execute(array(
                 ':id' => null,
                 ':unique_id'       => uniqid(),
                 ':verification_id' => uniqid(),
                 ':username'        =>  $this->user_name,
                 ':password'        => $this->password,
                 ':email'           =>  $this->email,
                 ':is_active'       => 0,
                 ':is_admin'        =>  0,
                 ':created'         => 30/07/2016,
                 ':modified'        => 2/08/2016,
                 ':deleted'         => 8/22/2016,
             ));
             header('location:create.php');
             $_SESSION['message'] = "You are successfully submited";
         }catch(PDOException $e){
             echo 'Error: ' . $e->getMessage();
         }
           }
        }
  }
    
    }
    
    
}
?>


