-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2016 at 12:09 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `td_registration`
--

CREATE TABLE `td_registration` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `verification_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_registration`
--

INSERT INTO `td_registration` (`id`, `unique_id`, `verification_id`, `username`, `password`, `email`, `is_active`, `is_admin`, `created`, `modified`, `deleted`) VALUES
(28, '579c489a761e0', '579c489a761e0', 'faruk', 'yyy', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, '579c5cf1843c2', '579c5cf1843c2', 'faruk', '123', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, '579c5dd0dffa3', '579c5dd0dffa3', 'faruk', '123', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, '579c5fc6aa317', '579c5fc6aa317', 'ooo', '123', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, '579c60e89c6b7', '579c60e89c6b7', 'pp', '123', 'vgujjar44@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, '579c628a2f429', '579c628a2f429', 'faruk', '123', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, '579c632d60a88', '579c632d60a88', 'farukgfhgg', '123', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, '579c658fe43b1', '579c658fe43b1', 'faruk', '123', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, '579c689a5b655', '579c689a5b655', 'far', '1', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, '579c78b64a65b', '579c78b64a65b', 'gfhfghoo', '1', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, '579c78dfab1b0', '579c78dfab1b0', 'ooooooo', '1', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, '579c79123027d', '579c79123027d', 'ppppppp', '1', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, '579c7bf1d51a6', '579c7bf1d51a6', 'eeeeeee', '4', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `td_registration`
--
ALTER TABLE `td_registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `td_registration`
--
ALTER TABLE `td_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
