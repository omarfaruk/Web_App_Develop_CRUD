-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2016 at 10:25 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lab`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `title` varchar(111) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `course_type` varchar(50) NOT NULL,
  `course_fee` varchar(111) NOT NULL,
  `is_offer` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `unique_id`, `title`, `duration`, `description`, `course_type`, `course_fee`, `is_offer`, `created`, `updated`, `deleted`) VALUES
(1, '', 'php', '3 months', 'All about php', 'free', 'paid', 1, '2016-08-12 00:00:00', '2016-08-12 00:00:00', '2016-08-12 00:00:00'),
(2, '57af3c1308109', '', '', '', '', '', 0, '2016-08-13 09:26:11', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '57af3d80c7354', '', '', '', '', '', 0, '2016-08-13 09:32:16', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, '57af3e2b51176', '', '', '', '', '', 0, '2016-08-13 09:35:07', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, '57af3e39c61bd', '', '', '', '', '', 0, '2016-08-13 09:35:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, '57af3e8cee682', '', '', '', '', '', 0, '2016-08-13 09:36:44', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, '57af3ee1edf3a', '', '', '', '', '', 0, '2016-08-13 09:38:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, '57af415d26543', '', '', '', '', '', 0, '2016-08-13 09:48:45', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, '57af4298b0e54', '', '', '', '', '', 0, '2016-08-13 09:54:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `course_trainer_lab_mapping`
--

CREATE TABLE `course_trainer_lab_mapping` (
  `id` int(11) NOT NULL,
  `course_id` int(111) NOT NULL,
  `batch_no` varchar(111) NOT NULL,
  `lead_trainer` varchar(111) NOT NULL,
  `asst_trainer` varchar(111) NOT NULL,
  `lab_asst` varchar(111) NOT NULL,
  `lab_id` int(111) NOT NULL,
  `start_date` varchar(111) NOT NULL,
  `ending_date` varchar(111) NOT NULL,
  `start_time` varchar(111) NOT NULL,
  `ending_time` varchar(111) NOT NULL,
  `day` varchar(111) NOT NULL,
  `is_running` int(11) NOT NULL,
  `assigned_by` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `installed_softwares`
--

CREATE TABLE `installed_softwares` (
  `id` int(11) NOT NULL,
  `labinfo_id` int(111) NOT NULL,
  `software_title` varchar(111) NOT NULL,
  `version` varchar(111) NOT NULL,
  `software_type` varchar(111) NOT NULL,
  `is_delete` tinyint(4) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `installed_softwares`
--

INSERT INTO `installed_softwares` (`id`, `labinfo_id`, `software_title`, `version`, `software_type`, `is_delete`, `created`, `updated`, `deleted`) VALUES
(3, 301, 'NetBeans', '8.0.2', 'free', 1, '0000-00-00 00:00:00', '2016-08-14 09:08:57', '0000-00-00 00:00:00'),
(11, 301, 'SubLime Text', '3', 'free', 1, '2016-08-12 08:08:54', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(12, 302, 'Netbeans', '8.0.1', 'free', 1, '0000-00-00 00:00:00', '2016-08-13 04:08:38', '0000-00-00 00:00:00'),
(13, 402, 'Netbeans', '8.0.1', 'free', 1, '2016-08-12 06:08:57', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 420, 'SubLime Text', '3', 'free', 1, '2016-08-12 06:08:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(16, 512, 'ZenStudio', '2016', 'free', 1, '2016-08-12 11:08:59', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(17, 512, 'NetBean', '2016', 'free', 1, '2016-08-13 07:08:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(18, 301, 'NetBean', '2016', 'free', 1, '2016-08-13 01:08:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(19, 402, 'PhpStorm', '2016', 'paid', 1, '2016-08-13 03:08:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(20, 402, 'PhpStorm', '2016', 'paid', 1, '2016-08-13 03:08:41', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 512, 'ZenStudio', '109', 'paid', 1, '0000-00-00 00:00:00', '2016-08-14 10:08:59', '0000-00-00 00:00:00'),
(22, 402, 'NetBean', '2016', 'paid', 1, '2016-08-13 03:08:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 402, 'NetBean', '2016', 'free', 1, '2016-08-13 04:08:24', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(24, 402, 'NetBean', '2016', 'paid', 1, '2016-08-13 04:08:23', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(25, 402, 'Netbeans', '8.0.1', 'paid', 1, '2016-08-13 04:08:52', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(26, 402, 'NetBean', '2016', 'paid', 1, '2016-08-13 04:08:33', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(27, 512, 'Sublime Text', '2016', 'free', 1, '2016-08-13 04:08:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(28, 420, 'PhpStorm', '2016', 'free', 1, '2016-08-13 04:08:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(29, 402, 'NetBean', '2016', 'free', 1, '2016-08-13 04:08:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(30, 402, 'NetBean', '2016', 'free', 1, '2016-08-13 04:08:08', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(31, 301, 'PhpStorm', '10', 'free', 1, '2016-08-13 04:08:14', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(32, 301, 'PhpStorm', '10', 'free', 1, '2016-08-13 04:08:22', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(33, 420, 'PhpStorm', '10', 'free', 1, '2016-08-13 04:08:39', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(34, 420, 'NetBean', '2016', 'paid', 1, '2016-08-13 04:08:09', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(35, 402, 'ZenStudio', '10', 'free', 1, '2016-08-13 04:08:35', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(36, 402, 'PhpStorm', '2016', 'free', 1, '2016-08-13 04:08:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(37, 512, 'NetBean', '3', 'free', 1, '2016-08-13 04:08:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(39, 301, 'PhpStorm', '2016', 'free', 1, '2016-08-13 04:08:27', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(40, 402, 'NetBean', '2016', 'free', 1, '2016-08-13 04:08:21', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(43, 402, 'NetBean', '2016', 'free', 0, '2016-08-14 09:08:29', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(44, 402, 'Notepad++', '2.2', 'free', 0, '2016-08-14 09:08:12', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(45, 301, 'Notepad++', '2016', 'paid', 1, '2016-08-14 09:08:32', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 402, 'xsdfvds', 'sdfs', 'paid', 1, '2016-08-14 10:08:50', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 301, 'sdf', 'sdf', 'paid', 0, '2016-08-14 10:08:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 301, 'dsf', 'sdsd', 'free', 0, '2016-08-14 10:08:06', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `labinfo`
--

CREATE TABLE `labinfo` (
  `id` int(11) NOT NULL,
  `course_id` int(100) NOT NULL,
  `lab_no` varchar(111) NOT NULL,
  `seat_capacity` varchar(111) NOT NULL,
  `projector_resolution` varchar(111) NOT NULL,
  `ac_status` varchar(111) NOT NULL,
  `pc_configuration` varchar(255) NOT NULL,
  `os` varchar(255) NOT NULL,
  `trainer_pc_configuration` varchar(255) NOT NULL,
  `table_capacity` varchar(100) NOT NULL,
  `internet_speed` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `labinfo`
--

INSERT INTO `labinfo` (`id`, `course_id`, `lab_no`, `seat_capacity`, `projector_resolution`, `ac_status`, `pc_configuration`, `os`, `trainer_pc_configuration`, `table_capacity`, `internet_speed`, `created`, `updated`, `deleted`) VALUES
(1, 201, '301', '30', '1200*1000', 'Yes', 'Brand PC', 'windows', 'Mac', '20', '15MB', '2016-08-12 00:00:00', '2016-08-12 00:00:00', '2016-08-12 00:00:00'),
(2, 202, '402', '30', '100*120', 'yes', 'brand', 'windows', 'mac', '21', '15mb', '2016-08-03 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 202, '420', '30', '120', 'yes', 'fasf', 'fdas', 'fasf', '033', '15mb', '2016-08-12 00:00:00', '2016-08-12 00:00:00', '2016-08-12 00:00:00'),
(4, 21513, '512', '30', '', '', '', '', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `trainers`
--

CREATE TABLE `trainers` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `edu_status` varchar(255) NOT NULL,
  `team` varchar(111) NOT NULL,
  `courses_id` int(11) NOT NULL,
  `trainer_status` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(111) NOT NULL,
  `address` varchar(255) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `web` varchar(111) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainers`
--

INSERT INTO `trainers` (`id`, `unique_id`, `full_name`, `edu_status`, `team`, `courses_id`, `trainer_status`, `image`, `phone`, `email`, `address`, `gender`, `web`, `created`, `updated`, `deleted`) VALUES
(1, '', 'Abu Taleb', 'BSC', 'Elite', 245, 'lab_assist', '', '01723689866', 'sumonmhd@gmail.com', 'Kustia', 'Male', 'sumonmhd.com', '2016-08-12 00:00:00', '2016-08-12 00:00:00', '2016-08-12 00:00:00'),
(2, '', 'Zadid Sir', 'MSC', 'Elite', 121, 'assist_trainer', '', '01723689866', 'fer@gmail.com', '', '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '', 'Zadid Sir', 'gsfsf', '11', 111, 'lead_trainer', 'adsa', '01723689866', 'dasd', 'dsd', 'dasd', 'dsa', '2016-08-12 00:00:00', '2016-08-12 00:00:00', '2016-08-12 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(111) NOT NULL,
  `full_name` varchar(111) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(111) NOT NULL,
  `password` varchar(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(10) NOT NULL,
  `is_admin` int(10) NOT NULL,
  `is_delete` int(10) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `unique_id`, `full_name`, `username`, `email`, `password`, `image`, `is_active`, `is_admin`, `is_delete`, `created`, `updated`, `deleted`) VALUES
(1, '', 'Ferdous', 'Ferd', 'ferdous1440@gmail.com', '123456', '', 1, 1, 0, '2016-08-03 00:00:00', '2016-08-12 00:00:00', '2016-08-12 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course_trainer_lab_mapping`
--
ALTER TABLE `course_trainer_lab_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `labinfo`
--
ALTER TABLE `labinfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainers`
--
ALTER TABLE `trainers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `installed_softwares`
--
ALTER TABLE `installed_softwares`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `labinfo`
--
ALTER TABLE `labinfo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `trainers`
--
ALTER TABLE `trainers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
