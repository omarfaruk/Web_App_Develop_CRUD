-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2016 at 04:18 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `semister`
--

-- --------------------------------------------------------

--
-- Table structure for table `td_semister`
--

CREATE TABLE `td_semister` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `semister` varchar(255) NOT NULL,
  `offer` varchar(255) NOT NULL,
  `cost` varchar(255) NOT NULL,
  `weaver` varchar(255) NOT NULL,
  `total` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_semister`
--

INSERT INTO `td_semister` (`id`, `name`, `semister`, `offer`, `cost`, `weaver`, `total`, `unique_id`) VALUES
(32, 'imran', '1st_semister', 'yes', '9000', '900', '8100', '578fcf32e8538 '),
(35, 'mamun', '3rd_semister', 'yes', '13000', '1300', '11700', '578fd331c0a77 '),
(36, 'omar', '2nd_semister', 'yes', '11000', '1100', '9900', '578fd33e7d347 '),
(37, 'babul', '3rd_semister', 'yes', '13000', '1300', '11700', '578fd42fe1417 ');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `td_semister`
--
ALTER TABLE `td_semister`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `td_semister`
--
ALTER TABLE `td_semister`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
