<?php

//include '../../../../Src/Bitm/SEIP124286/Semister/Semister.php';
include_once '../../../../vendor/autoload.php';
use App\Bitm\SEIP124286\Semister\Semister;
$obj = new Semister();
$mydata = $obj->index();

    if(isset($_SESSION['message']) && !empty($_SESSION['message'])){
    echo $_SESSION['message'];
    unset($_SESSION['message']);
}

?>

<a href="../../../../index.php">Project list</a> |

<a href="create.php"> Select Name & Semister  </a> 


<div><span>Search/ Filter</span>
    <span id="utility">Download as <a href="pdf.php" target="_blank">PDF</a> | <a href="xl.php" target="_blank">XL</a></span>
</div>

<table border="1px solid #ddd;">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Semister</th>
        <th>Offer</th>
        <th>Cost</th>
        <th>Weaver</th>
        <th>Total</th>
        <th>UniqueId</th>
        <th colspan="3">Action</th>
    </tr>
    
    <?php 
    $i = 1;
    
    if(isset($mydata) && !empty($mydata)){
    
    foreach ($mydata as $value) { ?>
    
    
    <tr>
        <td><?php echo $i++;?></td>
        <td><?php echo $value['name'];?></td>
        <td><?php echo $value['semister'];?></td>
        <td><?php echo $value['offer'];?></td>
        <td><?php echo $value['cost'];?></td>
        <td><?php echo $value['weaver'];?></td>
        <td><?php echo $value['total'];?></td>
        <td><?php echo $value['unique_id'];?></td>
      
    
        <td><a href="show.php?id=<?php echo $value['unique_id'];?>">View</a></td>
        <td><a href="edit.php?id=<?php echo $value['unique_id'];?>">Edit</a></td>
        <td><a href="delete.php?id=<?php echo $value['unique_id'];?>">Delete</a></td>
    </tr>
    
    <?php } } ?>
    
</table>
