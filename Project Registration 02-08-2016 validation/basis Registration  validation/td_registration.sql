-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2016 at 05:38 AM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `registration`
--

-- --------------------------------------------------------

--
-- Table structure for table `td_registration`
--

CREATE TABLE IF NOT EXISTS `td_registration` (
  `id` int(11) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `verification_id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `deleted` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `td_registration`
--

INSERT INTO `td_registration` (`id`, `unique_id`, `verification_id`, `username`, `password`, `email`, `is_active`, `is_admin`, `created`, `modified`, `deleted`) VALUES
(28, '579c489a761e0', '579c489a761e0', 'faruk', 'yyy', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, '579c5cf1843c2', '579c5cf1843c2', 'faruk', '123', 'omarfarukppi@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, '579d6f95cb601', '579d6f95cb604', 'omarfaruk', '44', 'omarfaruk@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, '579d7136e67bb', '579d7136e67be', 'fdshdfgujh', '123', 'omarfaruk@gmail.com', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, '579d7185e0a5f', '579d7185e0a62', 'sAGfdayg', '123', 'omarfaruk@gmail.com', 0, 0, '2016-07-31 05:33:25', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, '579d71d241624', '579d71d241626', 'dsdfxzvx', '123456', 'abc@gmail.com', 0, 0, '2016-07-31 09:34:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `td_registration`
--
ALTER TABLE `td_registration`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `td_registration`
--
ALTER TABLE `td_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
